<?php

namespace Drupal\Tests\google_top_headlines\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test form the module configurations.
 *
 * @group google_top_headlines
 */
class ConfigurationTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['google_top_headlines'];

  /**
   * Tests the configuration form, the permission and the link.
   */
  public function testConfigurationForm() {
    // Going to the config page.
    $this->drupalGet('/admin/config/google_top_headlines/adminsettings');

    // Checking that the page is not accesible for anonymous users.
    $this->assertSession()->statusCodeEquals(403);

    // Creating a user with the module permission.
    $account = $this->drupalCreateUser(['administer google_top_headlines', 'access administration pages']);
    // Log in.
    $this->drupalLogin($account);

    // Checking the module link.
    $this->drupalGet('/admin/config/system');
    $this->assertSession()->linkByHrefExists('/admin/config/google_top_headlines/adminsettings');

    // Going to the config page.
    $this->drupalGet('/admin/config/google_top_headlines/adminsettings');
    // Checking that the request has succeeded.
    $this->assertSession()->statusCodeEquals(200);
    // Check that the checkboxes are unchecked.
    $this->assertSession()->checkboxNotChecked('author_news');
    // Check that the checkboxes are unchecked.
    $this->assertSession()->checkboxNotChecked('title_news');
    // Check that the checkboxes are unchecked.
    $this->assertSession()->checkboxNotChecked('description_news');
    // Check that the checkboxes are unchecked.
    $this->assertSession()->checkboxNotChecked('link_news');
    // Check that the checkboxes are unchecked.
    $this->assertSession()->checkboxNotChecked('image_news');
    // Check that the checkboxes are unchecked.
    $this->assertSession()->checkboxNotChecked('publishedAt');
    // Check that the checkboxes are unchecked.
    $this->assertSession()->checkboxNotChecked('content_news');

    // Form values to send (checking checked checkboxes).
    $edit = [
      'author_news' => 1,
      'title_news' => 1,
      'description_news' => 1,
      'link_news' => 1,
      'image_news' => 1,
      'publishedAt' => 1,
      'content_news' => 1,
    ];
    // Sending the form.
    $this->drupalPostForm(NULL, $edit, 'op');

    // Getting the config factory service.
    $config_factory = $this->container->get('config.factory');

    // Getting variables.
    $google_top_headlines_form = $config_factory->get('google_top_headlines.settings')->get('google_api_key');

    // Verifiying that the config values are stored.
    $this->assertTrue($google_top_headlines_form, t('The configuration value for google_api_key should be TRUE.'));

    // Form values to send (checking unchecked checkboxes).
    $edit = [
      'author_news' => 0,
      'title_news' => 0,
      'description_news' => 0,
      'link_news' => 0,
      'image_news' => 0,
      'publishedAt' => 0,
      'content_news' => 0,
    ];

    // Sending the form.
    $this->drupalPostForm(NULL, $edit, 'op');

    // Getting variables.
    $google_top_headlines_form = $config_factory->get('google_top_headlines.settings')->get('google_api_key');
    // Verifiying that the config values are stored.
    $this->assertFalse($google_top_headlines_form, t('The configuration value for google_api_key should be FALSE.'));

  }

}
