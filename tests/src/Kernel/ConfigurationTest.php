<?php

namespace Drupal\Tests\google_top_headlines\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test the module configurations.
 *
 * @group google_top_headlines
 */
class ConfigurationTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['google_top_headlines'];

  /**
   * Tests the default configuration values.
   */
  public function testDefaultConfigurationValues() {
    // Installing the configuration file.
    $this->installConfig(ConfigurationTest::$modules);
    // Getting the config factory service.
    $config_factory = $this->container->get('config.factory');
    // Getting variable.
    $google_top_headlines_form = $config_factory->get('google_top_headlines.settings')->get('google_api_key');
    // Checking that the configuration variable is FALSE.
    $this->assertFalse($google_top_headlines_form, t('The default configuration value for google_api_key should be FALSE.'));
  }

}
