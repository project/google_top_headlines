CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The google top headlines provides the latest google headlines.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/google_top_headlines

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/google_top_headlines


REQUIREMENTS
------------

No special requirements


RECOMMENDED MODULES
-------------------

No any recommended modules


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
   for further information.


CONFIGURATION
-------------

 * Configure the module in Administration » Configuration » System »
   Google Top Headline Setting:

   - Enter google credentials/ keys.
   - Select check box to see that field in google top heading blocks



MAINTAINERS
-----------

Current maintainers:
 * Chandravilas Kute(chandravilas01) - https://www.drupal.org/u/chandravilas01

 